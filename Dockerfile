FROM openjdk:16-jdk-alpine
MAINTAINER mikhail
COPY target/*.jar config-server.jar
ENTRYPOINT ["java","-jar", "/config-server.jar"]
# config-server

## Spring Cloud Config Server

Spring Cloud Config provides server-side and client-side support for externalized configuration in a distributed system.  
A git repository https://gitlab.com/peopleflow-test-group/employee-management.git is a property source.

## Build

```
mvn clean package  
docker build -t mishgun8ku/config-server:latest .  
docker push mishgun8ku/config-server:latest  
docker run -d --rm --name config-server -p8888:8888 mishgun8ku/config-server:latest
```